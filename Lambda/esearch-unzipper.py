import boto3
import urllib.parse
import zipfile
from io import BytesIO


def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(
        event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    s3_resource = boto3.resource('s3')
    s3_client = boto3.client('s3')
    zip_obj = s3_resource.Object(bucket_name=bucket, key=key)
    buffer = BytesIO(zip_obj.get()["Body"].read())
    z = zipfile.ZipFile(buffer)
    for filename in z.namelist():
        file_info = z.getinfo(filename)
        file_folder = filename.split('.')[0]
        try:
            s3_client.head_object(
                Bucket=bucket,
                Key=f'bulkdata/{file_folder}/{filename}',
            )
        except Exception as e:
            print(e)
            s3_client.put_object(
                Body=z.open(file_info),
                Bucket=bucket,
                Key=f'bulkdata/{file_folder}/{filename}',
                ContentType='xml'
            )
            s3_client.delete_object(
                Bucket=bucket,
                Key=f'bulkdata/{file_folder}/{file_folder}.zip'
            )
