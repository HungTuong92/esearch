import boto3
import csv

client = boto3.client("s3")
resource = boto3.resource('s3')

BUCKET_NAME = 'esearch-resource'
RAW_DATA_PREFIX = 'bulkdata/'
METADATA_PREFIX = 'metadata/elasticsearch/'


def get_s3_key(prefix):
    if prefix == RAW_DATA_PREFIX:
        result = client.list_objects(
            Bucket=BUCKET_NAME, Prefix=prefix, Delimiter="/")
        paths = [subfolder.get("Prefix")
                 for subfolder in result.get("CommonPrefixes")]
        return [i.split('/')[-2] for i in paths]
    else:
        mybucket = resource.Bucket(BUCKET_NAME)
        files = mybucket.objects.filter(Prefix=prefix)
        files = [files.key for files in files]
        return list(filter(lambda x: 'csv' in x, files))


def retrieve_meta_records(bucket, key):
    try:
        meta_record = client.get_object(Bucket=bucket, Key=key)
        data = meta_record['Body'].read().decode('utf-8').splitlines()
        lines = csv.reader(data)
        headers = next(lines)  # skip header
        for line in lines:
            if line[1] == 'Done':
                return line[0]
    except Exception:
        return


def lambda_handler(event, context):
    executed_files_key = get_s3_key(METADATA_PREFIX)

    valid_executed_files = [retrieve_meta_records(
        BUCKET_NAME, f'{key}') for key in executed_files_key]
    all_files = get_s3_key(RAW_DATA_PREFIX)

    files_to_process = list(set(all_files) - set(valid_executed_files))
    number_of_files = len(files_to_process)

    key1 = key2 = 'None'

    if len(files_to_process) >= 2:
        key1 = files_to_process[0]
        key2 = files_to_process[1]
    if len(files_to_process) == 1:
        key1 = files_to_process[0]

    return {
        'unprocessed_files': number_of_files,
        'key1': key1,
        'key2': key2
    }
