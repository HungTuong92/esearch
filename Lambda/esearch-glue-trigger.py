import boto3
import csv
import time
import random

glue = boto3.client('glue')
client = boto3.client("s3")
resource = boto3.resource('s3')

BUCKET_NAME = 'esearch-resource'
RAW_DATA_PREFIX = 'bulkdata/'
METADATA_PREFIX = 'metadata/'
OVERRIDE = False
GLUE_JOB = 'esearch-parse-xml'


def get_s3_key(prefix):
    if prefix == RAW_DATA_PREFIX:
        result = client.list_objects(
            Bucket=BUCKET_NAME, Prefix=prefix, Delimiter="/")
        paths = [subfolder.get("Prefix")
                 for subfolder in result.get("CommonPrefixes")]
        return [i.split('/')[-2] for i in paths]
    else:
        mybucket = resource.Bucket(BUCKET_NAME)
        files = mybucket.objects.filter(Prefix=prefix)
        files = [files.key for files in files]
        return list(filter(lambda x: 'csv' in x, files))


def retrieve_meta_records(bucket, key):
    try:
        meta_record = client.get_object(Bucket=bucket, Key=key)
        data = meta_record['Body'].read().decode('utf-8').splitlines()
        lines = csv.reader(data)
        headers = next(lines)  # skip header
        for line in lines:
            if line[1] == 'Done':
                return line[0]
    except Exception:
        return


def execute_glue(file):
    run_job = glue.start_job_run(
        JobName=GLUE_JOB,
        Arguments={
            '--BUCKET_NAME': BUCKET_NAME,
            '--OUTPUT_BUCKET_NAME': BUCKET_NAME,
            '--FILE': file
        },
    )
    status = glue.get_job_run(JobName=GLUE_JOB, RunId=run_job['JobRunId'])
    print(
        f"Job Status : {status['JobRun']['JobRunState']}  |  Job Id: { status['JobRun']['Id']}")


def retry_with_backoff(fn, file, retries=3, backoff_in_seconds=1):
    x = 0
    while True:
        try:
            return fn(file)
        except:
            if x == retries-1:
                raise
            else:
                sleep = (backoff_in_seconds * 2 ** x + random.uniform(0, 1))
                time.sleep(sleep)
                x += 1


def proceed(file_list):
    for file in file_list:
        retry_with_backoff(execute_glue, file)


def select_job(opt=''):
    global METADATA_PREFIX
    global GLUE_JOB

    if opt == 'parse':
        METADATA_PREFIX = 'metadata/parse-xml/'
        GLUE_JOB = 'esearch-parse-xml'
    if opt == 'validation':
        METADATA_PREFIX = 'metadata/validation/'
        GLUE_JOB = 'esearch-validation'


def lambda_handler(event, context):
    select_job('parse')
    executed_files_key = get_s3_key(METADATA_PREFIX)

    valid_executed_files = [retrieve_meta_records(
        BUCKET_NAME, f'{key}') for key in executed_files_key]
    all_files = get_s3_key(RAW_DATA_PREFIX)

    files_to_process = list(set(all_files) - set(valid_executed_files))
    # print(len(files_to_process))

    meta_exists = len(valid_executed_files) != 0
    if not meta_exists:
        proceed(all_files)
    if OVERRIDE and meta_exists:
        proceed(all_files)
    if not OVERRIDE and meta_exists:
        proceed(files_to_process)

    return {
        'unprocessed_files': len(files_to_process)
    }
