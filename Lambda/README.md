# AWS Lambda

AWS Lambda to proceed the process of the Esearch project.

## AWS Lambda Description

- ```esearch-unzipper```: unzip file whenever the `bulkdata/` folder receives one.
- ```esearch-glue-trigger```: perform calling glue job to run concurrently for parsing and validation.
- ```esearch-files-producer```: find failed or unprocessed files to feed into the Step Functions for ingesting into the Elasticsearch cluster.

## AWS Lambda configuration

- ```esearch-unzipper``` :  
  - timeout: 5 min
  - memory: 512 MB
- ```esearch-glue-trigger``` :  
  - timeout: 15 min
  - memory: 128 MB
- ```esearch-files-producer``` :
  - timeout: 3 min
  - memory: 128 MB
