# Esearch Project

## How it works

- If you work on a new Elasticsearch account, clear all metadata folders in s3 path `s3://esearch-resource/metadata/elasticsearch/`.
- Then add necessary information about your Elasticsearch cluster in Job Parameters in Glue Job `glue-ingest-data` include (cloud_id, username, password).
- Then execute the step functions `MyStateMachine`.
- Check number of failed after finish pipeline by running lambda `esearch-files-producers`.
- Config .env and run `main.py` to check number of ingested records in your cluster.

## Step functions flow

![screenshot](./img/flow.png)
