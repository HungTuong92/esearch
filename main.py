from elasticsearch import Elasticsearch
from pprint import pprint
import os
from dotenv import load_dotenv

load_dotenv()

CLOUD_ID = os.getenv('CLOUD_ID')
USER = os.getenv('USER')
PASSWORD = os.getenv('PASSWORD')

es = Elasticsearch(
    cloud_id=CLOUD_ID,
    basic_auth=(USER, PASSWORD)
)

# remove entire index on Elasticsearch

# es.indices.refresh()
# res = es.indices.delete(index='products')
# pprint(res)

# count number of documents in index
es.indices.refresh()
res = es.count(index='products')
pprint(res)

# query random 30 documents in the index
result = es.search(
    index='products',
    body={
        "size": 30,
        "query": {
            "function_score": {
                "functions": [{
                    "random_score": {
                        "seed": "1518707649"
                    }
                }]
            }
        }
    }
)

if result['hits']['hits']:
    pprint(result['hits']['hits'])
