# AWS Glue

AWS Glue script to ingest validated data to Elasticsearch cluster.

## AWS Glue Job Configuration

- Glue version: Glue 2.0
- Language: Python 3
- Worker type: G.1X
- Requested number of workers: 2
- Job Bookmark: Disable
- Number of retries: 0
- Job timeout (minutes): 2880
- Maximum concurrency: 5
- Python libs: `elasticsearch.zip`, `elastic_transport.zip`

## AWS Glue Job parameters

- --INPUT_BUCKET_NAME : esearch-resource
- --OUTPUT_BUCKET_NAME: esearch-resource
- --FILE: apc220220 (test-only)
- --CLOUD_ID: found_on_elasticsearch
- --USERNAME: elastic
- --PASSWORD: found_on_elasticsearch
- --INDEX: `products`
- --class: GlueApp
