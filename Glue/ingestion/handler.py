import sys
import os
from elasticsearch import Elasticsearch, helpers
import pyspark.sql.functions as F
import pyspark.sql.types as T
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "BUCKET_NAME",
    "OUTPUT_BUCKET_NAME",
    "FILE",

    "CLOUD_ID",
    "USERNAME",
    "PASSWORD",
    "INDEX"
])

sc = SparkContext()
glueContext = GlueContext(sc)

spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

BUCKET_NAME = args["BUCKET_NAME"]
OUTPUT_BUCKET_NAME = args["OUTPUT_BUCKET_NAME"]
FILE = args["FILE"]
CLOUD_ID = args["CLOUD_ID"]
USERNAME = args["USERNAME"]
PASSWORD = args["PASSWORD"]
INDEX = args["INDEX"]

# Avoid spark from generating marker files $folders$
hadoop_conf = sc._jsc.hadoopConfiguration()
hadoop_conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")

# create es session
es = Elasticsearch(
    cloud_id=CLOUD_ID,
    basic_auth=(USERNAME, PASSWORD)
)


def write_to_s3(df, path, format='csv'):
    if format == 'csv':
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").csv(path)
    else:
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").parquet(path)


def convert_df(df):
    groups = {
        'classifications': [
            'international_code',
            'first_use_anywhere_date',
            'first_use_in_commerce_date'
        ],
        'owners': [
            'legal_entity_type_code',
            'party_type',
            'party_name',
            'address_1',
            'address_2',
            'city',
            'state',
            'country',
            'postcode'
        ]
    }

    text_df = (df
               .groupBy("serial_number")
               .agg(F.collect_list(F.col('text')).alias('text'))
               .withColumn('text', F.explode(F.col('text')))
               .filter((F.col('text') != ''))
               .groupBy("serial_number")
               .agg(F.collect_list(F.col('text')).alias('text'))
               )

    classification_df = (df
                         .groupBy("serial_number")
                         .agg(F.collect_list(F.struct(*groups['classifications'])).alias('classifications'))
                         .withColumn('classifications', F.explode(F.col('classifications')))
                         .filter((F.col('classifications.international_code') != '') |
                                 (F.col('classifications.first_use_anywhere_date') != '') |
                                 (F.col('classifications.first_use_in_commerce_date') != ''))
                         .groupBy("serial_number")
                         .agg(F.collect_list(F.col('classifications')).alias('classifications'))
                         )

    owner_df = (df
                .groupBy("serial_number")
                .agg(F.collect_list(F.struct(*groups['owners'])).alias('owners'))
                .withColumn('owners', F.explode(F.col('owners')))
                .filter((F.col('owners.legal_entity_type_code') != '') |
                        (F.col('owners.party_type') != '') |
                        (F.col('owners.party_name') != '') |
                        (F.col('owners.address_1') != '') |
                        (F.col('owners.address_2') != '') |
                        (F.col('owners.city') != '') |
                        (F.col('owners.state') != '') |
                        (F.col('owners.country') != '') |
                        (F.col('owners.postcode') != ''))
                .groupBy("serial_number")
                .agg(F.collect_list(F.col('owners')).alias('owners'))
                )

    # select cols
    cols = [
        'serial_number',
        'registration_number',
        'filing_date',
        'registration_date',
        'mark_identification',
        'attorney_name'
    ]
    df = df.select(cols).dropDuplicates()

    df = df.join(text_df, ['serial_number'], how='left')
    df = df.join(classification_df, ['serial_number'], how='left')
    df = df.join(owner_df, ['serial_number'], how='left')

    df = df.groupBy("serial_number").agg(F.collect_list(
        F.struct(*df.columns)).alias('json_record'))

    return df


def create_index_if_not_exist():
    try:
        settings = {
            "index": {
                "number_of_shards": 5,
                "number_of_replicas": 1
            }
        }
        res = es.indices.create(index=INDEX, settings=settings)
    except Exception as e:
        return


def es_add_bulk(records):
    json_data = ({
        "_index": INDEX,
        "_id": int(record['serial_number']),
        "_source": record,
    } for record in records)

    counter = 0
    for success, info in helpers.parallel_bulk(es, json_data, thread_count=os.cpu_count(), chunk_size=12500):
        if success:
            counter += 1
    return counter


def process():
    if FILE == 'None':
        return

    schema = T.StructType([
        T.StructField('file', T.StringType()),
        T.StructField('status', T.StringType()),
        T.StructField('missing_number', T.IntegerType()),
    ])

    df = spark.read.format("parquet").option('header', True).load(
        f"s3://{BUCKET_NAME}/validation/valid/{FILE}/")
    df = convert_df(df)

    # create list of json records
    records = df.select("json_record").collect()
    records = [record.asDict(recursive=True)['json_record'][0]
               for record in records]

    # push json to Elasticsearch
    create_index_if_not_exist()
    try:
        ingested_records = es_add_bulk(records)
        meta_df = spark.createDataFrame(
            [(FILE, "Done", (len(records) - ingested_records))], schema)
        # save file
        write_to_s3(
            meta_df, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/elasticsearch/{FILE}/csv/", format='csv')
    except Exception as e:
        # save file
        meta_df = spark.createDataFrame(
            [(FILE, "Failed", len(records))], schema)
        write_to_s3(
            meta_df, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/elasticsearch/{FILE}/csv/", format='csv')


process()
