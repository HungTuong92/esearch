import sys
import traceback
import pyspark.sql.functions as F
import pyspark.sql.types as T
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job


args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "BUCKET_NAME",
    "OUTPUT_BUCKET_NAME",
    "FILE"
])

sc = SparkContext()
glueContext = GlueContext(sc)

spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

BUCKET_NAME = args["BUCKET_NAME"]
OUTPUT_BUCKET_NAME = args["OUTPUT_BUCKET_NAME"]
FILE = args["FILE"]

# Avoid spark from generating marker files $folders$
hadoop_conf = sc._jsc.hadoopConfiguration()
hadoop_conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")


def flatten(schema, prefix=None):
    """Flattens out nested schema"""
    fields = []
    for field in schema.fields:
        name = f"{prefix}.{field.name}" if prefix else field.name
        dtype = field.dataType
        if isinstance(dtype, T.ArrayType):
            dtype = dtype.elementType
        if isinstance(dtype, T.StructType):
            fields += flatten(dtype, prefix=name)
        else:
            fields.append(name)
    return fields


def write_to_s3(df, path, format='csv'):
    if format == 'csv':
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").csv(path)
    else:
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").parquet(path)


def save_meta(df, file):
    write_to_s3(
        df, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/parse-xml/{file}/csv", format='csv')
    write_to_s3(
        df, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/parse-xml/{file}/parquet", format='parquet')


def process(file):
    schema_cols = ["filename", "status", "error"]
    success_meta = spark.createDataFrame([(file, "Done", "None")], schema_cols)
    df = spark.read.format("xml").options(
        rowTag="case-file").load(f"s3://{BUCKET_NAME}/bulkdata/{file}/")
    try:
        cols = [
            "serial-number",
            "registration-number",
            "case-file-header.filing-date",
            "case-file-header.registration-date",
            "case-file-header.mark-identification",
            "case-file-header.attorney-name",
            "case-file-statements.case-file-statement.text",
            "classifications.classification.international-code",
            "classifications.classification.first-use-anywhere-date",
            "classifications.classification.first-use-in-commerce-date",
            "case-file-owners.case-file-owner.legal-entity-type-code",
            "case-file-owners.case-file-owner.party-type",
            "case-file-owners.case-file-owner.party-name",
            "case-file-owners.case-file-owner.address-1",
            "case-file-owners.case-file-owner.address-2",
            "case-file-owners.case-file-owner.city",
            "case-file-owners.case-file-owner.state",
            "case-file-owners.case-file-owner.country",
            "case-file-owners.case-file-owner.postcode",
        ]

        # output schema cols
        output_cols = [i.split('.')[-1] for i in cols]
        output_cols = [i.replace('-', '_') for i in output_cols]

        # get value from nested columns
        flatten_schema = flatten(df.schema)
        common_cols = list(set(cols).intersection(flatten_schema))
        missing_cols = [x for x in cols if x in cols and x not in common_cols]

        df = df.select(common_cols)
        for col in missing_cols:
            df = df.withColumn(
                col.split(".")[-1], F.lit(None).cast(T.StringType()))

        for col in df.columns:
            # rename cols:
            new_col = col.replace("-", "_")
            df = df.withColumnRenamed(col, new_col)

        # check array columns in schema
        nested_cols = [
            "international_code",
            "first_use_anywhere_date",
            "first_use_in_commerce_date",
            "legal_entity_type_code",
            "party_type",
            "party_name",
            "address_1",
            "address_2",
            "city",
            "state",
            "country",
            "postcode",
            "text",
        ]

        list_col = []
        for col in nested_cols:
            if 'array' in df.select(col).dtypes[0][1]:
                list_col.append(col)

        # filter all null records in classification, statement and owners
        null_df = df.where((F.col('text').isNull()) &
                           (F.col('international_code').isNull()) &
                           (F.col('first_use_anywhere_date').isNull()) &
                           (F.col('first_use_in_commerce_date').isNull()) &
                           (F.col('legal_entity_type_code').isNull()) &
                           (F.col('party_type').isNull()) &
                           (F.col('party_name').isNull()) &
                           (F.col('address_1').isNull()) &
                           (F.col('address_2').isNull()) &
                           (F.col('city').isNull()) &
                           (F.col('state').isNull()) &
                           (F.col('country').isNull()) &
                           (F.col('postcode').isNull()))

        df = df.exceptAll(null_df)
        for col in list_col:
            null_df = null_df.withColumn(col, F.explode(col))

        # process the rest of df
        not_list_col = list(set(df.columns) - set(list_col))
        if list_col:
            df = (df.withColumn("tmp", F.arrays_zip(*list_col))
                    .withColumn("tmp", F.explode("tmp"))
                    .select(*not_list_col, *[F.col(f'tmp.{x}') for x in list_col]))

        # rearrange columns
        null_df = null_df.select(output_cols)
        df = df.select(output_cols)

        # concat 2 df vertically
        df = df.union(null_df)

        # change types
        for col in df.columns:
            df = df.withColumn(col, F.col(col).cast(T.StringType()))

        # replace null values
        df = df.fillna('', subset=df.columns)
        for col in df.columns:
            df = df.withColumn(col, F.regexp_replace(F.col(col), 'None', ''))

        date_cols = [
            'filing_date',
            'registration_date',
            'first_use_anywhere_date',
            'first_use_in_commerce_date'
        ]

        for col in date_cols:
            df = df.withColumn(col, F.regexp_replace(F.col(col), '0', ''))

        # drop dup
        df = df.dropDuplicates()

        # save file
        write_to_s3(
            df, path=f"s3://{OUTPUT_BUCKET_NAME}/parse-xml/{file}/", format='parquet')
        save_meta(success_meta, file)
    except Exception as e:
        csv_meta_err = spark.createDataFrame(
            [(file, "Failed", "Error")], schema_cols)
        write_to_s3(
            csv_meta_err, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/parse-xml/{file}/csv/", format='csv')

        parquet_meta_err = spark.createDataFrame(
            [(file, "Failed", str(traceback.format_exc()))], schema_cols)
        write_to_s3(
            parquet_meta_err, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/parse-xml/{file}/parquet/", format='parquet')


process(FILE)
