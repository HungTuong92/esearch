# AWS Glue

AWS Glue script to download `bulkdata` zip files to S3 bucket.

## AWS Glue Job Configuration

- Glue version: Glue 2.0
- Language: Python 3
- Worker type: G.1X
- Requested number of workers: 2
- Job Bookmark: Disable
- Number of retries: 0
- Job timeout (minutes): 2880
- Maximum concurrency: 1
- Python libs: `bs4.zip`

## AWS Glue Job parameters

- --INPUT_BUCKET_NAME : esearch-resource
- --OUTPUT_BUCKET_NAME: esearch-resource
- --META_KEY_PATH: metadata/downloader/
- --SOURCE_PATH: <https://bulkdata.uspto.gov/data/trademark/dailyxml/applications/>
