import sys
import boto3
import requests
import random
import time
import traceback
from concurrent.futures import ThreadPoolExecutor
from bs4 import BeautifulSoup
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrame
from awsglue.job import Job
import pyspark
import pyspark.sql.functions as F
from pyspark.sql import types as T


args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "INPUT_BUCKET_NAME",
    "OUTPUT_BUCKET_NAME",
    "META_KEY_PATH",
    "SOURCE_PATH",
])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)
logger = glueContext.get_logger()

INPUT_BUCKET_NAME = args['INPUT_BUCKET_NAME']
OUTPUT_BUCKET_NAME = args['OUTPUT_BUCKET_NAME']
META_KEY_PATH = args['META_KEY_PATH']
SOURCE_PATH = args['SOURCE_PATH']

# Avoid spark from generating marker files $folders$
hadoop_conf = sc._jsc.hadoopConfiguration()
hadoop_conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")


def is_metadata(prefix):
    """Returns T/F whether the file exists."""
    bucket = boto3.resource('s3').Bucket(INPUT_BUCKET_NAME)
    metafile = list(bucket.objects.filter(Prefix=prefix))
    return len(metafile) == 2 and prefix in metafile[1].key


def get_source_files():
    # # check if metadata exists
    # if is_metadata(META_KEY_PATH):
    #     df = spark.read.format('parquet').load(f"s3://{OUTPUT_BUCKET_NAME}/{META_KEY_PATH}")
    #     rows = df.where(F.col('status') == 'failed').select('filename').collect()
    #     failed_filename = [row['filename'] for row in rows]
    #     return [(filename, SOURCE_PATH + filename + '.zip') for filename in failed_filename]

    # crawl file name
    res = requests.get(SOURCE_PATH)
    soup = BeautifulSoup(res.text, 'html.parser')
    files = soup.find_all('tr')[4:]
    lst = [file.td.a.get_text() for file in files]
    filename_lst = list(map(lambda x: x.split('.')[0], lst))

    return filename_lst


def download_single(filename, url):
    OUTPUT_BUCKET_NAME = args['OUTPUT_BUCKET_NAME']

    # create new session for each thread
    s3_client = boto3.session.Session().client('s3')
    try:
        s3_client.head_object(
            Bucket=OUTPUT_BUCKET_NAME,
            Key=f'bulkdata/{filename}/{filename}.xml',
        )
    except Exception as e:
        time.sleep(random.uniform(0.5, 2.5))
        res = requests.get(url)
        if res.status_code == 200:
            s3_client.put_object(
                Body=res.content,
                Bucket=OUTPUT_BUCKET_NAME,
                Key=f'bulkdata/{filename}/{filename}.zip'
            )


def execute_downloader(filename, url):
    '''
    Function that was passed down to worker to execute downloader
    Input: a pySpark row
    '''
    cols = ['filename', 'url', 'status', 'error']
    try:
        download_single(filename, url)
        return spark.createDataFrame([(filename, url, 'done', "None")], cols)
    except Exception as err:
        return spark.createDataFrame([(filename, url, 'failed', str(traceback.format_exc()))], cols)


# Schema for the dataframe
schema = T.StructType([
    T.StructField('filename', T.StringType()),
    T.StructField('url', T.StringType()),
    T.StructField('status', T.StringType()),
    T.StructField('error', T.StringType()),
])

# Main download script
emptyRDD = spark.sparkContext.emptyRDD()
result_df = spark.createDataFrame(emptyRDD, schema)
files_to_download = get_source_files()

if is_metadata(META_KEY_PATH):
    df = spark.read.format('parquet').load(
        f"s3://{OUTPUT_BUCKET_NAME}/{META_KEY_PATH}")
    result_df = result_df.union(df.where(F.col('status') == 'done'))

    # remove succeeded files out of the list
    downloaded_file_rows = df.where(
        F.col('status') == 'done').select(F.col('filename')).collect()
    downloaded_files = [row['filename'] for row in downloaded_file_rows]
    files_to_download = list(set(files_to_download) - set(downloaded_files))

    # append failed files to list of files to be downloaded
    failed_file_rows = df.where(F.col('status') == 'failed').select(
        F.col('filename')).collect()
    failed_files = [row['filename'] for row in failed_file_rows]
    files_to_download += failed_files


# convert list of files to be downloaded on source
files_to_download = [(filename, SOURCE_PATH + filename + '.zip')
                     for filename in files_to_download]

with ThreadPoolExecutor() as executor:
    for res in executor.map(lambda p: execute_downloader(*p), files_to_download):
        result_df = result_df.union(res)

result_df.coalesce(1).sort(F.col('filename')).write.mode("overwrite").option(
    "header", "true").parquet(f"s3://{OUTPUT_BUCKET_NAME}/{META_KEY_PATH}")
