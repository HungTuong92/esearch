# AWS Glue

AWS Glue script to validate data from parsed parquet files in S3 bucket.

## AWS Glue Job Configuration

- Glue version: Glue 2.0
- Language: Python 3
- Worker type: G.1X
- Requested number of workers: 2
- Job Bookmark: Disable
- Number of retries: 0
- Job timeout (minutes): 2880
- Maximum concurrency: 200

## AWS Glue Job parameters

- --BUCKET_NAME : esearch-resource
- --OUTPUT_BUCKET_NAME: esearch-resource
- --FILE: apc220220 (test-only)
- --class: GlueApp
