import sys
import traceback
import pyspark.sql.functions as F
import pyspark.sql.types as T
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "BUCKET_NAME",
    "OUTPUT_BUCKET_NAME",
    "FILE"
])

sc = SparkContext()
glueContext = GlueContext(sc)

spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

BUCKET_NAME = args["BUCKET_NAME"]
OUTPUT_BUCKET_NAME = args["OUTPUT_BUCKET_NAME"]
FILE = args["FILE"]

# Avoid spark from generating marker files $folders$
hadoop_conf = sc._jsc.hadoopConfiguration()
hadoop_conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")


def write_to_s3(df, path, format='csv'):
    if format == 'csv':
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").csv(path)
    else:
        df.coalesce(1).write.mode("overwrite").option(
            "header", "true").parquet(path)


def data_validation(df):
    # serial number
    df = df.filter(~F.isnan(F.col('serial_number')) &
                   F.col('serial_number').isNotNull())
    df = df.where(F.col('serial_number').rlike('^[0-9]+$'))
    df = df.where(F.length(F.col('serial_number')) == 8)

    # registration number
    df = df.where((F.col('registration_number').rlike('^[0-9]*$')))

    # mark identification
    df = df.where((F.col('mark_identification').rlike('^[a-zA-Z0-9 ]*$')))

    # international code
    df = df.where(F.col('international_code').rlike('^[0-9\[\]]*$'))

    # legal entity type code
    df = df.where(F.length(F.col('legal_entity_type_code')) <= 2)

    # party type
    df = df.where((F.length(F.col('party_type')).isin([0, 2])) & (
        (F.col('party_type').rlike('^[0-9]*$'))))

    # address
    df = df.where(F.length(F.col('address_1')) <= 40)
    df = df.where(F.length(F.col('address_2')) <= 40)

    # city
    df = df.where(F.length(F.col('city')) <= 40)

    # state
    df = df.where((F.length(F.col('state')).isin([0, 2, 3])) & (
        F.col('state').rlike(r'^[a-zA-Z]*$')))

    # country
    df = df.where((F.length(F.col('country')).isin([0, 2, 3])) & (
        F.col('country').rlike(r'^[a-zA-Z]*$')))

    # postcode
    df = df.where((F.length(F.col('postcode')) <= 15))

    # date
    date_cols = [
        'filing_date',
        'registration_date',
        'first_use_anywhere_date',
        'first_use_in_commerce_date'
    ]
    for col in date_cols:
        df = df.where((F.col(col).rlike('^[0-9]*$')))

    return df


def drop_reason(df):
    df = df.withColumn('drop_reason',
                       F.when((F.isnan(F.col('serial_number'))) | (
                           F.col('serial_number').isNull()), 'no serial number')
                       .when(~(F.col('serial_number').rlike('^[0-9]+$')), 'invalid serial number type')
                       .when((F.length(F.col('serial_number')) != 8), 'invalid serial number length')
                       .when(~(F.col('registration_number').rlike('^[0-9]*$')), 'invalid registration number type')
                       .when(~(F.col('mark_identification').rlike('^[a-zA-Z0-9 ]*$')), 'invalid mark identification type')
                       .when(~(F.col('international_code').rlike('^[0-9\[\]]*$')), 'invalid international code type')
                       .when(~(F.length(F.col('legal_entity_type_code')) <= 2), 'invalid legal entity type code length')
                       .when(~(F.col('party_type').rlike('^[0-9]*$')), 'invalid party type')
                       .when(~(F.length(F.col('party_type')).isin([0, 2])), 'invalid party type length')
                       .when(~(F.length(F.col('address_1')) <= 40), 'invalid address 1 length')
                       .when(~(F.length(F.col('address_2')) <= 40), 'invalid address 2 length')
                       .when(~(F.length(F.col('city')) <= 40), 'invalid city length')
                       .when(~(F.length(F.col('state')).isin([0, 2, 3])) & (F.col('state').rlike(r'^[a-zA-Z]*$')), 'invalid state type')
                       .when(~(F.length(F.col('country')).isin([0, 2, 3])) & (F.col('country').rlike(r'^[a-zA-Z]*$')), 'invalid country type')
                       .when(~(F.length(F.col('postcode')) <= 15), 'invalid postcode length')
                       .when(~(F.col('filing_date').rlike('^[0-9]*$')), 'invalid filing date type')
                       .when(~(F.col('registration_date').rlike('^[0-9]*$')), 'invalid registration date type')
                       .when(~(F.col('first_use_anywhere_date').rlike('^[0-9]*$')), 'invalid first use anywhere date type')
                       .when(~(F.col('first_use_in_commerce_date').rlike('^[0-9]*$')), 'invalid first use in commerce date type')
                       .otherwise('unidentified'))
    return df


def change_datetype(df):
    df = df.withColumn("filing_date",  F.to_date(F.unix_timestamp(F.col("filing_date").cast(T.StringType()), "yyyyMMdd").cast(T.TimestampType()))) \
        .withColumn("registration_date",  F.to_date(F.unix_timestamp(F.col("registration_date").cast(T.StringType()), "yyyyMMdd").cast(T.TimestampType()))) \
        .withColumn("first_use_anywhere_date",  F.to_date(F.unix_timestamp(F.col("first_use_anywhere_date").cast(T.StringType()), "yyyyMMdd").cast(T.TimestampType()))) \
        .withColumn("first_use_in_commerce_date",  F.to_date(F.unix_timestamp(F.col("first_use_in_commerce_date").cast(T.StringType()), "yyyyMMdd").cast(T.TimestampType())))
    return df


def process(file):
    schema_cols = ["filename", "status", "error"]

    df = spark.read.format("parquet").load(
        f"s3://{BUCKET_NAME}/parse-xml/{file}/")

    try:
        valid_df = data_validation(df)
        invalid_df = df.exceptAll(valid_df)

        # add reason column to invalid records
        invalid_df = drop_reason(invalid_df)

        # change type
        valid_df = change_datetype(valid_df)
        invalid_df = change_datetype(invalid_df)

        # save file
        write_to_s3(
            valid_df, path=f"s3://{OUTPUT_BUCKET_NAME}/validation/valid/{file}/", format='parquet')
        write_to_s3(
            invalid_df, path=f"s3://{OUTPUT_BUCKET_NAME}/validation/invalid/{file}/", format='parquet')

        df_meta = spark.createDataFrame([(file, "Done", "None")], schema_cols)
        write_to_s3(
            df_meta, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/validation/{file}/csv/", format='csv')
    except Exception as e:
        df_meta = spark.createDataFrame(
            [(file, "Failed", str(traceback.format_exc()))], schema_cols)
        write_to_s3(
            df_meta, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/validation/{file}/parquet/", format='parquet')

        df_meta = spark.createDataFrame(
            [(file, "Failed", "Error")], schema_cols)
        write_to_s3(
            df_meta, path=f"s3://{OUTPUT_BUCKET_NAME}/metadata/validation/{file}/csv/", format='csv')


process(FILE)
