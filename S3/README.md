# AWS S3
AWS S3 trigger a lambda to unzip .zip file whenever it is put into the `buldkata/` folder. 

## AWS S3 Organization

- ```bulkdata/```: contains unzipped data from source.
- ```parse-xml/```: contains parsed xml files.
- ```validation/```: contains valid and invalid records from parse-xml files.
- ```metadata/```: contains metadata of each process.
- ```python-libs/```: contains python libs and jars.

## AWS S3 Event notifications

- Name : esearch-unzip-event
- Event types: Put, Post
- Filters: bulkdata/, .zip
- Destination type: Lambda function
- Destination: esearch-unzipper
