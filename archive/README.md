# Esearch Data Validation

Validate between data from S3 and data from source [UPSTO Bulk Data](https://bulkdata.uspto.gov/data/trademark/dailyxml/applications/).

## Project Organization

- ```\data```: contains example data from S3.
- ```\source```: contains corresponding data from source.
- ```main.py```: contains code.
- ```bulkdata.html```: static html page of source.

## Workflow

```python
if __name__ == "__main__":
    # check missing files between source and database
    file_missing_result()

    # if apc18840407-20201231-01 file is not missing, check its data validation
    validate('apc18840407-20201231-01')
```
The flow is divided into 2 parts: 
- Check missing files between user's databases (in this case ```apc18840407-20201231-01```) and source:
  * ```python
    def file_missing_result():
    ```
  The function will call ```get_source_files()``` to get all available files of source
  from response to URL which is saved as ```bulkdata.html``` and ```get_data_files()``` to get all 
  files in the database (assume). Then the function compare 2 lists of files and print out results.
- If a file exists on source, validate its content by:
  * Download the source file from the URL, unzip and store in ```\source```.
  * Generate .csv file from the .xml format of the source file by ```generate_xml2csv()```.
  * Change type to match with the data file.
  * Open data file and drop duplicates, open source file and only select columns corresponding with data file.
  * Check whether the number of records in data file match with source file.
  * Check which record is different from source file by divide 2 dataframe into chunks and 
  process them in parallel in ```multiproc()``` and compare each chunks in ```compare_df_chunks()``` 
  and return different records.

