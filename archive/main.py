import pickle
import pandas as pd
import multiprocessing
import os
import urllib.request
import zipfile
import concurrent.futures as cf
import numpy as np
import time
from xml.etree import ElementTree as ET
from bs4 import BeautifulSoup
from datetime import datetime
from collections import deque

curr_dir = os.getcwd()
data_path = os.path.join(curr_dir, 'data')
source_path = os.path.join(curr_dir, 'source')
filename = 'bulkdata.html'
url = "https://bulkdata.uspto.gov/data/trademark/dailyxml/applications/"
processed_file_name = 'processed.pkl'


def crawl_html():
    if filename in os.listdir(curr_dir):
        return
    html_dir = os.path.join(curr_dir, filename)
    urllib.request.urlretrieve(url, html_dir)
    return


def get_source_files():
    crawl_html()
    soup = BeautifulSoup(
        open(os.path.join(curr_dir, filename)), features='lxml')

    files = soup.find_all('tr')[4:]
    lst = [file.td.a.get_text() for file in files]
    return list(map(lambda x: x.split('.')[0], lst))


def get_data_files():
    return [file.split('.')[0] for file in os.listdir(data_path)]


def file_missing_result():
    source = get_source_files()
    data_files = get_data_files()
    missing_files = list(filter(lambda x: x not in source, data_files))
    if len(missing_files) == 0:
        print('All data is available on source.')
        return
    print('There are {} missing files on sources.'.format(len(missing_files)))
    [print('{}'.format(file)) for file in missing_files]


def str_date(x):
    if x == 'None':
        return x
    return datetime.strptime(x, "%Y%m%d").strftime("%m-%d-%Y")


def generate_xml2csv(filename):
    file = source_path + '\\' + filename + '.xml'
    tree = ET.parse(file)
    root = tree.getroot()

    date_cols = [
        'transaction_date',
        'registration_date',
        'status_date',
        'filing_date',
        'location_date',
        'renewal_date'
    ]

    rows = []
    for casefile in root.findall('.//case-file'):
        dict = {'serial-number': int(casefile.find('serial-number').text),
                'registration-number': int(casefile.find('registration-number').text),
                'transaction-date': casefile.find('transaction-date').text}
        for ele in casefile.find('case-file-header'):
            dict[ele.tag] = ele.text
        rows.append(dict)

    cols = list(rows[0].keys())
    df = pd.DataFrame(rows, columns=cols)

    # Transform df type
    df = df.astype(str)
    df.rename(columns=lambda x: x.replace('-', '_'), inplace=True)
    df.drop('color_drawing_current_in', axis='columns', inplace=True)

    for col in df.columns:
        df[col].replace({'nan': 'None'}, inplace=True)

    for col in date_cols:
        if col in df.columns:
            df[col] = df[col].apply(str_date)

    return df


def compare_df_chunks(df1, df2):
    df1 = df1.astype("string")
    df2 = df2.astype("string")
    return df1[df1.eq(df2).all(axis=1) == False]


def multiproc(df1, df2):
    cpus = multiprocessing.cpu_count()
    procs = deque()
    df1_splits = np.array_split(df1, cpus)
    df2_splits = np.array_split(df2, cpus)

    with cf.ProcessPoolExecutor(max_workers=cpus) as executor:
        for df1_slice, df2_slice in zip(df1_splits, df2_splits):
            procs.append(
                executor.submit(compare_df_chunks, df1_slice, df2_slice)
            )

    return (future.result() for future in cf.as_completed(procs))


def compare_columns(file, df1, df2, df1_name, df2_name):
    '''
    Compare schema between 2 df
    '''
    folder_path = os.path.join(curr_dir, file)
    report_path = os.path.join(curr_dir, file, 'report.txt')
    flag = True
    for col in df1.columns:
        if col not in df2.columns:
            flag = False
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
                with open(report_path, 'w') as outfile:
                    outfile.write(f'Column {col} from {df1_name} not exist in {df2_name}\n')
            else:
                with open(report_path, 'a') as outfile:
                    outfile.write(f'Column {col} from {df1_name} not exist in {df2_name}\n')
    
    for col in df2.columns:
        if col not in df1.columns:
            flag = False
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
                with open(report_path, 'w') as outfile:
                    outfile.write(f'Column {col} from {df2_name} not exist in {df1_name}\n')
            else:
                with open(report_path, 'a') as outfile:
                    outfile.write(f'Column {col} from {df2_name} not exist in {df1_name}\n')

    return flag


def validate(file, processed_files):
    # download and extract zip file
    zip_file = file.replace('.csv', '') + '.zip'
    file_url = url + zip_file
    # save the source file
    save_path = source_path + '\\' + zip_file
    if not zip_file in os.listdir(source_path):
        urllib.request.urlretrieve(file_url, save_path)
        with zipfile.ZipFile(save_path, "r") as zip_ref:
            zip_ref.extractall(source_path)
    os.remove(save_path)

    data_df = generate_xml2csv(file)
    data_df = data_df.drop_duplicates()
    cols = data_df.columns

    source_df = pd.read_csv(data_path + '\\' + file + '.csv', low_memory=False)
    is_same_schema = compare_columns(file, data_df, source_df, 'remote_source', 's3_source')
    if not is_same_schema:
        source_df = source_df[[c for c in cols if c in source_df.columns]]

    if source_df.shape[0] > data_df.shape[0]:
        print('Data is missing records from source.')
    else:
        res = multiproc(data_df, source_df)
        df3 = pd.DataFrame()
        for df_slice in res:
            df3 = pd.concat([df3, df_slice])
        print('Incorrect records in the {} file:\n{}'.format(file + '.csv', df3.shape[0]))
        
        # write diff file
        if not df3.empty:
            folder_path = os.path.join(curr_dir, file)
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            df3.to_csv(os.path.join(folder_path, f'{file}-diff.csv'))
            data_df.to_csv(os.path.join(folder_path, f'{file}-data.csv'))
            source_df.to_csv(os.path.join(folder_path, f'{file}-source.csv'))
    
    processed_files.append(file)
    save_processed_files(processed_files)
    xml_file = file.replace('.csv', '') + '.xml'
    # save the source file
    save_path = source_path + '\\' + xml_file
    os.remove(save_path)


def get_processed_files():
    if processed_file_name in os.listdir(curr_dir):
        with open(processed_file_name, 'rb') as infile:
            return pickle.load(infile)
    return []


def save_processed_files(processed_file):
    with open(processed_file_name, 'wb') as outfile:
        pickle.dump(processed_file, outfile)


if __name__ == "__main__":
    start_time = time.time()
    # check missing files between source and database
    file_missing_result()

    # get processed file
    processed_files = get_processed_files()

    # if file is not missing, check its data validation
    total = len(os.listdir(data_path))
    for i, file in enumerate(os.listdir(data_path)):
        file = file.replace('.csv', '')
        if file in processed_files:
            continue
        print('Validating......', file, f'{i+1}/{total}')
        validate(file, processed_files)
    # print("--- %s seconds ---" % (time.time() - start_time))